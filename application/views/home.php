<?php
$var = $this->config->item("unraid_vars");
?>
    <section id="dashboard">
		<?php $this->load->view( 'status', array( 'statustype' => 'full' ) ); ?>
        <section class="dashmodules">
        	<section class="system_info full">
             	<ul>
                	<li><span class="greentext">SERVER</span> <?php echo $var["NAME"];?> - <?php echo $uptime;?></li>
                	<li><span class="greentext">LOAD</span> <?php echo $load;?></li>
                	<li><span class="greentext">MODEL</span> <?php echo $cpuinfo;?></li>
                    <li><span class="greentext">MEMORY</span> <?php echo $memory;?></li>
                    <li><span class="greentext">IP ADDRESS</span> <?php echo $var["IPADDR"];?></li>
            	</ul>
           
            </section>
        </section>

        
        <section class="section1">

            <!--<section class="array_drives">
                <div class="drive_list">
                    <ul>
                    	<li>
                        
							<?php if(isset($disks["parity"]) && !empty($disks["parity"])) { ?>
                            <?php $drive_details = drive_details( $disks["parity"] ); ?>
                            <div class="inset-box disk-info">
                            	<div class="drivecol"><?php echo spin_disk($disks["parity"]["name"],$disks["parity"]["idx"],"url");?></div>
                                <div class="disk-ref">
                                    <h2><?php echo $disks["parity"]["name"];?></h2>
                                    <div class="disk_size"><?php echo $drive_details['size'];?></div>
                                    <div class="disk-name"><?php echo $drive_details['id'];?></div>
                                    <div class="temp"><?php echo $drive_details["temp"];?><span>&deg;C</span></div>
                                    <div class="temp<?php if( $drive_details['errors'] > 0 ) echo ' redtext';?>"><?php echo $drive_details['errors'];?> <span>Error<?php echo( $drive_details['errors']=='1' ) ? '' : 's';?></span></div>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="inset-box disk-info no-disk">
                             	<div class="disk-ref">Parity</div>
                                <i class="icon-hdd disk"></i>
                                <div></div>
                                <div class="temp">No Parity Drive</div>
                            </div>
                
                            <?php } ?>
                        
                        
                        
                        </li>
                    	<li class="alt">
                        
							<?php if(isset($disks["cache"]) && !empty($disks["cache"])) { ?>
                            <?php $drive_details = drive_details( $disks["cache"] ); ?>
                            <div class="inset-box disk-info">
                            	<div class="drivecol"><?php echo spin_disk($disks["cache"]["name"],$disks["cache"]["idx"],"url");?></div>
                                <div class="disk-ref">
                                    <h2><?php echo $disks["cache"]["name"];?></h2>
                                    <div class="disk_size"><?php echo $drive_details['size'];?></div>
                                    <div class="disk-name"><?php echo $drive_details['id'];?></div>
                                    <div class="temp"><?php echo $drive_details["temp"];?><span>&deg;C</span></div>
                                    <div class="temp<?php if( $drive_details['errors'] > 0 ) echo ' redtext';?>"><?php echo $drive_details['errors'];?> <span>Error<?php echo( $drive_details['errors']=='1' ) ? '' : 's';?></span></div>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="inset-box disk-info no-disk">
                             	<div class="disk-ref">Cache</div>
                                <i class="icon-hdd disk"></i>
                                <div></div>
                                <div class="temp">No Cache Drive</div>
                            </div>
                
                            <?php } ?>
                        
                        
                        
                        </li>
                    	<li>
                        
							<?php if(isset($disks["flash"]) && !empty($disks["flash"])) { ?>
                            <?php $drive_details = drive_details( $disks["flash"] ); ?>
                            <div class="inset-box disk-info">
                            	<div class="drivecol"><?php echo spin_disk($disks["flash"]["name"],$disks["parity"]["idx"],"url", true);?></div>
                                <div class="disk-ref">
                                    <h2><?php echo $disks["flash"]["name"];?></h2>
                                    <div class="disk_size"><?php echo $drive_details['size'];?></div>
                                    <div class="disk-name"><?php echo $var["flashGUID"]." (".$disks["flash"]["device"].")";?></div>
                                    <div class="temp"><?php echo $drive_details["temp"];?><span>&deg;C</span></div>
                                    <div class="temp<?php if( $drive_details['errors'] > 0 ) echo ' redtext';?>"><?php echo $drive_details['errors'];?> <span>Error<?php echo( $drive_details['errors']=='1' ) ? '' : 's';?></span></div>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="inset-box disk-info no-disk">
                             	<div class="disk-ref">Flash</div>
                                <i class="icon-hdd disk"></i>
                                <div></div>
                                <div class="temp">No Flash Drive</div>
                            </div>
                
                            <?php } ?>
                        
                        
                        
                        </li>
                       
                    </ul>
                </div>
            </section>--><section id="datadisks" class="body">
            	<div class="datadisks">
                    <div class="tabs">
                        <ul>
                            <li class="active"><a href="#tabs-1">Attention Required <span class="lightertext">(<?php echo $warn_count;?>)</span></a></li><li>
                            <a href="#tabs-2">All Disks <span class="lightertext">(<?php echo $all_count;?>)</span></a></li><li>
                            <a href="#tabs-3">Non-array Disks (<?php echo $narray_disk_count;?>)</a></li><li>
                            <a href="#tabs-4">Options</a></li>
                        </ul>
                        <div class="tab_container">
                            <div class="addontab active" id="tabs-1">
                                <div class="inner">
                                    <!-- items needing attention -->
                                    <?php echo $warning_list; ?>
                                </div>
                            </div>
                            <div class="addontab" id="tabs-2">
                                <div class="inner">
                                    <!-- all disks -->
                                    <?php echo $all_list; ?>
                                    
                                </div>
                            </div>
                            <div class="addontab" id="tabs-3">
                                <div class="inner">
                                    <!-- options -->
                                    <?php echo $narray_list; ?>
                                </div>
                            </div>
                            <div class="addontab" id="tabs-4">
                                <div class="inner">
                                    <!-- options -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            </section>
            
        
        </section>
        
        
    </section>

<form class="ajaxupdate" method="post" action="http://<?php echo current(explode(":", $_SERVER['HTTP_HOST']));?>/update.htm">
    <input type="hidden" name="cmdStatus" value="Apply" />
    <button type="submit">Update</button>
</form>